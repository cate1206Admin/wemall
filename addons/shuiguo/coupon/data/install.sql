SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `addons_common_coupon` (
  `id` int(10) NOT NULL COMMENT '自增ID',
  `coupon_menu_id` int(16) NOT NULL COMMENT '代金券分类ID',
  `code` varchar(255) NOT NULL COMMENT '代金券码',
  `status` int(11) NOT NULL COMMENT '1为已经使用',
  `price` int(16) NOT NULL COMMENT '代金卷价格',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `last_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '最后使用时间',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `addons_common_coupon`
--

INSERT INTO `addons_common_coupon` (`id`, `coupon_menu_id`, `code`, `status`, `price`, `user_id`, `last_time`, `created_at`, `updated_at`) VALUES
(1, 1, '271323', 0, 10, 0, '2017-03-23 14:35:00', '2017-03-02 19:13:11', '2017-04-20 03:58:13'),
(2, 1, '167747', 0, 10, 0, '2017-03-23 14:35:00', '2017-03-02 19:13:11', '2017-04-20 03:58:13'),
(3, 1, '515884', 0, 10, 0, '2017-03-23 14:35:00', '2017-03-02 19:13:11', '2017-04-20 03:54:38'),
(4, 1, '933758', 0, 10, 0, '2017-03-23 14:35:00', '2017-03-02 19:13:11', '2017-04-20 03:58:15'),
(5, 1, '311254', 0, 10, 0, '2017-03-23 14:35:00', '2017-03-02 19:13:11', '2017-04-20 03:55:04');

-- --------------------------------------------------------

--
-- 表的结构 `addons_common_coupon_active_config`
--

CREATE TABLE `addons_common_coupon_active_config` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(32) DEFAULT NULL COMMENT '活动标题',
  `start_time` timestamp NULL DEFAULT NULL COMMENT '开始时间',
  `last_time` timestamp NULL DEFAULT NULL COMMENT '结束时间',
  `probability` tinyint(3) DEFAULT NULL COMMENT '获取优惠券概率',
  `total_times` int(10) NOT NULL DEFAULT '0' COMMENT '每人总次数',
  `day_times` int(10) NOT NULL DEFAULT '0' COMMENT '每人每天总次数',
  `header_img` int(11) DEFAULT NULL COMMENT '活动图片',
  `use_desc` text COMMENT '使用说明',
  `active_desc` text COMMENT '活动说明',
  `status` tinyint(1) DEFAULT NULL COMMENT '-1:未开始 0:正在进行 1:已经结束'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `addons_common_coupon_active_config`
--

INSERT INTO `addons_common_coupon_active_config` (`id`, `title`, `start_time`, `last_time`, `probability`, `total_times`, `day_times`, `header_img`, `use_desc`, `active_desc`, `status`) VALUES
(1, '好玩的优惠券', '2017-04-18 05:45:00', '2017-04-27 03:50:00', 70, 10, 2, 214, '只限在本店使用', '奖品多多，欢迎参加', 0);

-- --------------------------------------------------------

--
-- 表的结构 `addons_common_coupon_change`
--

CREATE TABLE `addons_common_coupon_change` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL COMMENT '优惠券id',
  `score` text NOT NULL,
  `type` int(11) NOT NULL COMMENT '1:积分兑换2:手动发送',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `addons_common_coupon_change`
--

INSERT INTO `addons_common_coupon_change` (`id`, `user_id`, `coupon_id`, `score`, `type`, `created_at`) VALUES
(1, 1, 1, '20', 1, '2017-03-02 08:21:00'),
(2, 1, 2, '20', 1, '2017-04-19 03:55:22'),
(3, 1, 2, '0', 2, '2017-04-19 07:36:49'),
(4, 1, 3, '0', 2, '2017-04-19 07:39:27'),
(5, 1, 5, '0', 2, '2017-04-19 07:39:51'),
(6, 1, 2, '0', 2, '2017-04-19 07:42:55'),
(7, 1, 3, '0', 2, '2017-04-19 07:42:59'),
(8, 1, 4, '0', 2, '2017-04-19 07:43:02'),
(9, 1, 5, '0', 2, '2017-04-19 07:43:54'),
(10, 1, 2, '0', 2, '2017-04-19 08:16:35'),
(11, 1, 3, '0', 2, '2017-04-19 08:22:00'),
(12, 1, 4, '0', 2, '2017-04-19 08:32:22'),
(13, 1, 5, '0', 2, '2017-04-19 08:34:35'),
(14, 1, 2, '0', 2, '2017-04-19 08:38:11'),
(15, 1, 3, '0', 2, '2017-04-19 08:39:09'),
(16, 1, 4, '0', 2, '2017-04-19 08:40:36'),
(17, 1, 2, '0', 2, '2017-04-19 08:43:25'),
(18, 1, 3, '0', 2, '2017-04-19 08:46:28'),
(19, 1, 4, '0', 2, '2017-04-19 08:50:06'),
(20, 1, 5, '0', 2, '2017-04-20 01:10:36'),
(21, 1, 2, '0', 2, '2017-04-20 03:51:46'),
(22, 1, 3, '0', 2, '2017-04-20 03:51:50'),
(23, 1, 4, '0', 2, '2017-04-20 03:51:52'),
(24, 1, 5, '0', 2, '2017-04-20 03:51:53'),
(25, 1, 2, '0', 2, '2017-04-20 03:55:00');

-- --------------------------------------------------------

--
-- 表的结构 `addons_common_coupon_menu`
--

CREATE TABLE `addons_common_coupon_menu` (
  `id` int(16) NOT NULL COMMENT '优惠券ID',
  `name` varchar(255) NOT NULL COMMENT '优惠券名字',
  `price` varchar(255) NOT NULL COMMENT '优惠券价格',
  `score` int(11) NOT NULL COMMENT '兑换所需积分',
  `num` int(12) NOT NULL COMMENT '优惠券数量',
  `last_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '过期时间',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '-1:过期 1:开启0:关闭',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `addons_common_coupon_menu`
--

INSERT INTO `addons_common_coupon_menu` (`id`, `name`, `price`, `score`, `num`, `last_time`, `status`, `created_at`, `updated_at`) VALUES
(1, '端午节大酬宾', '10', 20, 5, '2017-07-23 14:35:00', 1, '2017-03-02 19:13:11', '2017-07-02 19:13:11');

-- --------------------------------------------------------

--
-- 表的结构 `addons_common_coupon_user_log`
--

CREATE TABLE `addons_common_coupon_user_log` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `total_use` int(11) NOT NULL DEFAULT '0' COMMENT '使用总次数',
  `day_use` int(11) NOT NULL DEFAULT '0' COMMENT '当天使用次数',
  `date` date DEFAULT NULL COMMENT '最后抽奖次数'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `addons_common_coupon_user_log`
--

INSERT INTO `addons_common_coupon_user_log` (`id`, `user_id`, `total_use`, `day_use`, `date`) VALUES
(1, 1, 0, 0, '2017-04-20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addons_common_coupon`
--
ALTER TABLE `addons_common_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addons_common_coupon_active_config`
--
ALTER TABLE `addons_common_coupon_active_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addons_common_coupon_change`
--
ALTER TABLE `addons_common_coupon_change`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addons_common_coupon_menu`
--
ALTER TABLE `addons_common_coupon_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addons_common_coupon_user_log`
--
ALTER TABLE `addons_common_coupon_user_log`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `addons_common_coupon`
--
ALTER TABLE `addons_common_coupon`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增ID', AUTO_INCREMENT=6;
--
-- 使用表AUTO_INCREMENT `addons_common_coupon_active_config`
--
ALTER TABLE `addons_common_coupon_active_config`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `addons_common_coupon_change`
--
ALTER TABLE `addons_common_coupon_change`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- 使用表AUTO_INCREMENT `addons_common_coupon_menu`
--
ALTER TABLE `addons_common_coupon_menu`
  MODIFY `id` int(16) NOT NULL AUTO_INCREMENT COMMENT '优惠券ID', AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `addons_common_coupon_user_log`
--
ALTER TABLE `addons_common_coupon_user_log`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
