-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: 2017-05-27 10:28:51
-- 服务器版本： 5.5.42
-- PHP Version: 5.5.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wemall7_empty`
--

-- --------------------------------------------------------

--
-- 表的结构 `admin`
--

CREATE TABLE `admin` (
  `id` int(10) unsigned NOT NULL COMMENT '用户ID',
  `group_id` int(11) NOT NULL DEFAULT '1' COMMENT '用户组id',
  `username` char(16) DEFAULT NULL COMMENT '用户名',
  `password` char(32) DEFAULT NULL COMMENT '密码',
  `email` char(32) DEFAULT NULL COMMENT '用户邮箱',
  `mobile` char(15) DEFAULT NULL COMMENT '用户手机',
  `reg_ip` varchar(20) NOT NULL DEFAULT '0' COMMENT '注册IP',
  `last_login_time` timestamp NULL DEFAULT NULL COMMENT '最后登录时间',
  `last_login_ip` text COMMENT '最后登录IP',
  `status` tinyint(4) DEFAULT '0' COMMENT '用户状态',
  `remark` text COMMENT '备注',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='后台用户表';

--
-- 转存表中的数据 `admin`
--

INSERT INTO `admin` (`id`, `group_id`, `username`, `password`, `email`, `mobile`, `reg_ip`, `last_login_time`, `last_login_ip`, `status`, `remark`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '296720094@qq.com', '18053449656', '0', '2017-05-26 10:00:24', '0.0.0.0', 1, '', '0000-00-00 00:00:00', '2017-05-26 10:00:23'),
(2, 1, '1', 'c4ca4238a0b923820dcc509a6f75849b', '1604583867@qq.com', '18538753627', '127.0.0.1', '0000-00-00 00:00:00', '2130706433', 1, '1', '0000-00-00 00:00:00', '2017-02-15 14:33:01'),
(3, 2, '2', '779d005fa526b871d424fcab8140582f', '1604583867@qq.com', '18538753627', '127.0.0.1', '0000-00-00 00:00:00', '0', 1, '1', '0000-00-00 00:00:00', '2017-01-17 01:25:23');

-- --------------------------------------------------------

--
-- 表的结构 `ads`
--

CREATE TABLE `ads` (
  `id` int(10) unsigned NOT NULL,
  `position_id` int(11) NOT NULL DEFAULT '0' COMMENT '广告位置',
  `name` text,
  `sub` text,
  `file_id` int(11) NOT NULL DEFAULT '0',
  `url` text,
  `remark` text,
  `rank` int(11) DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1:开启0:关闭',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ads`
--

INSERT INTO `ads` (`id`, `position_id`, `name`, `sub`, `file_id`, `url`, `remark`, `rank`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, '1', '1210', 108, '19', '1', 0, 1, '2015-11-06 06:58:06', '2017-03-03 03:11:27'),
(2, 2, '2', '', 98, '', '1', 0, 1, '2015-11-06 06:58:20', '2017-03-03 03:11:32'),
(3, 2, '3', '', 99, '', '1', 0, 1, '2015-11-06 06:58:30', '2017-03-03 03:11:35'),
(4, 2, '4', '', 100, '', '1', 333, 1, '2015-11-06 06:58:41', '2017-03-03 01:19:23'),
(6, 2, '广告测试', '2222', 97, '', '1', 222, 1, '2016-01-05 08:14:23', '2017-03-01 09:45:37'),
(7, 2, '111', '111', 95, '11', '111', 222, 1, '2017-03-02 08:35:42', '2017-03-03 03:11:19');

-- --------------------------------------------------------

--
-- 表的结构 `ads_position`
--

CREATE TABLE `ads_position` (
  `id` int(11) unsigned NOT NULL,
  `name` text,
  `sub` text,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ads_position`
--

INSERT INTO `ads_position` (`id`, `name`, `sub`, `status`, `created_at`, `updated_at`) VALUES
(1, '幻灯片', '', 1, '2016-12-15 14:42:54', '2017-02-13 02:23:00'),
(2, '首页广告', NULL, 1, '2017-02-17 03:27:55', '0000-00-00 00:00:00'),
(3, '快捷菜单', '', 1, '2017-03-15 07:46:10', '2017-03-15 07:46:10');

-- --------------------------------------------------------

--
-- 表的结构 `analysis`
--

CREATE TABLE `analysis` (
  `id` int(10) unsigned NOT NULL,
  `orders` int(11) NOT NULL DEFAULT '0',
  `trades` float NOT NULL DEFAULT '0',
  `registers` int(11) NOT NULL DEFAULT '0',
  `users` int(11) NOT NULL DEFAULT '0' COMMENT '当天购买人数',
  `date` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `analysis`
--

INSERT INTO `analysis` (`id`, `orders`, `trades`, `registers`, `users`, `date`, `created_at`, `updated_at`) VALUES
(1, 0, 0, 10, 23, '2017-01-03', '2015-11-15 21:57:10', '2016-08-16 15:56:59'),
(2, 3, 46, 12, 3, '2017-01-04', '2015-11-17 14:07:19', '2016-08-16 15:56:50'),
(3, 5, 180, 0, 5, '2016-08-07', '2015-11-18 11:05:35', '2016-08-16 15:56:47'),
(4, 9, 286, 0, 9, '2016-08-08', '2015-11-19 16:08:57', '2016-08-16 15:56:42'),
(5, 1, 36, 0, 1, '2016-08-09', '2015-11-22 15:06:03', '2016-08-16 15:56:37'),
(6, 2, 84, 0, 2, '2016-08-10', '2015-12-22 09:47:38', '2016-08-16 15:56:30'),
(7, 5, 90, 0, 5, '2016-08-11', '2015-12-23 10:49:23', '2016-08-16 15:56:25'),
(8, 65, 72, 0, 0, '2016-08-12', '2016-01-05 20:36:40', '2016-08-16 15:56:18'),
(9, 6, 48, 0, 0, '2016-08-13', '2016-01-06 14:45:39', '2016-08-16 15:56:14'),
(10, 2, 38, 0, 0, '2016-11-26', '2016-01-08 10:07:45', '2016-11-25 15:56:09'),
(11, 1, 24, 0, 0, '2016-11-28', '2016-01-17 11:41:34', '2016-11-27 15:56:04'),
(12, 12, 664, 5, 10, '2016-11-29', '2016-10-31 17:02:27', '2016-11-28 15:56:00'),
(13, 6, 404, 3, 0, '2016-11-30', '2016-08-17 17:02:09', '2016-11-29 11:55:58'),
(14, 0, 0, 0, 1, '2017-01-12', '2017-01-12 01:47:05', '2017-01-12 01:47:05'),
(15, 0, 0, 0, 1, '2017-01-13', '2017-01-12 19:35:11', '2017-01-12 19:35:11'),
(16, 0, 0, 0, 1, '2017-01-14', '2017-01-13 18:37:30', '2017-01-13 18:37:30'),
(17, 0, 0, 1, 1, '2017-01-16', '2017-01-15 18:40:55', '2017-01-16 00:51:10'),
(18, 0, 0, 1, 0, '2017-01-22', '2017-01-21 21:15:32', '2017-01-21 21:15:32'),
(19, 0, 0, 1, 1, '2017-02-08', '2017-02-07 22:37:37', '2017-02-08 05:55:10'),
(20, 0, 0, 0, 1, '2017-02-14', '2017-02-13 23:14:35', '2017-02-13 23:14:35'),
(21, 0, 0, 0, 1, '2017-02-16', '2017-02-15 17:11:59', '2017-02-15 17:11:59'),
(22, 0, 0, 1, 0, '2017-02-18', '2017-02-17 22:32:11', '2017-02-17 22:32:11'),
(23, 0, 0, 0, 1, '2017-02-21', '2017-02-21 03:35:45', '2017-02-21 03:35:45'),
(25, 0, 0, 1, 1, '2017-02-28', '2017-02-27 16:10:41', '2017-02-23 01:01:06'),
(26, 2, 3, 2, 3, '2017-03-01', '2017-02-28 21:12:38', '2017-02-24 02:45:18'),
(28, 5, 36, 0, 0, '2017-03-02', '2017-03-02 09:25:52', '2017-03-02 09:25:52'),
(29, 31, 2062, 0, 0, '2017-03-03', '2017-03-03 03:25:10', '2017-03-03 03:25:10'),
(30, 20, 1106, 4, 4, '2017-03-04', '2017-03-04 01:31:56', '2017-03-04 01:31:56'),
(31, 9, 577, 1, 9, '2017-03-06', '2017-03-06 06:19:20', '2017-03-06 06:19:20'),
(32, 50, 204.35, 1, 38, '2017-03-07', '2017-03-07 01:50:13', '2017-03-07 01:50:13'),
(33, 17, 0.17, 3, 5, '2017-03-08', '2017-03-08 01:11:27', '2017-03-08 01:11:27'),
(34, 0, 0, 1, 0, '2017-03-09', '2017-03-09 01:34:16', '2017-03-09 01:34:16'),
(35, 0, 0, 2, 0, '2017-03-10', '2017-03-10 06:30:19', '2017-03-10 06:30:19'),
(36, 0, 0, 1, 0, '2017-03-13', '2017-03-13 08:53:30', '2017-03-13 08:53:30');

-- --------------------------------------------------------

--
-- 表的结构 `article`
--

CREATE TABLE `article` (
  `id` int(10) unsigned NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `title` text,
  `author` text COMMENT '作者',
  `sub` text,
  `content` text,
  `remark` text,
  `visiter` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1:开启0:关闭',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `article`
--

INSERT INTO `article` (`id`, `category_id`, `title`, `author`, `sub`, `content`, `remark`, `visiter`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '关于我们', '大白', '简介', '<p>这里放关于我们内容</p>', '1', 11, 1, '2016-01-05 14:41:14', '2017-03-02 06:52:09');

-- --------------------------------------------------------

--
-- 表的结构 `article_category`
--

CREATE TABLE `article_category` (
  `id` int(11) unsigned NOT NULL,
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '上级',
  `name` text COMMENT '类型',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1:开启0:关闭',
  `remark` text COMMENT '备注',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='文章类型';

--
-- 转存表中的数据 `article_category`
--

INSERT INTO `article_category` (`id`, `pid`, `name`, `status`, `remark`, `created_at`, `updated_at`) VALUES
(1, 0, '帮助', 1, 'pc端页面下帮助', '2016-12-08 23:17:30', '2017-02-17 02:07:13');

-- --------------------------------------------------------

--
-- 表的结构 `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(8) unsigned NOT NULL,
  `title` char(100) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1:启用0:禁用',
  `rules` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `auth_group`
--

INSERT INTO `auth_group` (`id`, `title`, `status`, `rules`, `created_at`, `updated_at`) VALUES
(1, '超级管理员', 1, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,', '2017-01-16 01:28:11', '0000-00-00 00:00:00'),
(2, '普通管理员', 1, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58', '2017-01-16 06:59:52', '2017-02-15 09:08:55');

-- --------------------------------------------------------

--
-- 表的结构 `auth_group_access`
--

CREATE TABLE `auth_group_access` (
  `id` int(10) unsigned NOT NULL,
  `uid` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `auth_group_access`
--

INSERT INTO `auth_group_access` (`id`, `uid`, `group_id`) VALUES
(1, 1, 1),
(3, 2, 1),
(5, 4, 2),
(6, 5, 2),
(7, 6, 2),
(8, 7, 1);

-- --------------------------------------------------------

--
-- 表的结构 `auth_rule`
--

CREATE TABLE `auth_rule` (
  `id` mediumint(8) unsigned NOT NULL,
  `name` char(80) DEFAULT NULL,
  `title` char(20) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `rank` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `auth_rule`
--

INSERT INTO `auth_rule` (`id`, `name`, `title`, `type`, `rank`, `status`, `condition`, `created_at`, `updated_at`) VALUES
(1, 'admin/index/index', '系统首页', 1, 0, 1, '', '2017-01-09 08:37:37', '0000-00-00 00:00:00'),
(2, 'admin/analysis/user', '用户分析', 1, 0, 1, '', '2017-01-10 07:20:26', '0000-00-00 00:00:00'),
(3, 'admin/analysis/order', '订单分析', 1, 0, 1, '', '2017-01-10 07:25:40', '0000-00-00 00:00:00'),
(4, 'admin/analysis/product', '商品分析', 1, 0, 1, '', '2017-01-10 07:27:28', '0000-00-00 00:00:00'),
(5, 'admin/config.shop/index', '商城设置', 1, 0, 1, '', '2017-01-11 03:13:45', '0000-00-00 00:00:00'),
(6, 'admin/tpl.shop/index', '商城模板设置', 1, 0, 1, '', '2017-01-11 14:52:45', '0000-00-00 00:00:00'),
(7, 'admin/config.pay/index', '支付方式列表', 1, 0, 1, '', '2017-01-11 03:42:28', '0000-00-00 00:00:00'),
(8, 'admin/wx.config/index', '微信设置', 1, 0, 1, '', '2017-01-11 07:10:02', '0000-00-00 00:00:00'),
(9, 'admin/wx.menu/index', '微信菜单设置', 1, 0, 1, '', '2017-01-11 07:39:06', '0000-00-00 00:00:00'),
(10, 'admin/wx.menu/add', '新增修改微信菜单', 1, 0, 1, '', '2017-01-11 08:03:05', '0000-00-00 00:00:00'),
(11, 'admin/wx.menu/del', '删除微信菜单', 1, 0, 1, '', '2017-01-12 03:05:09', '0000-00-00 00:00:00'),
(12, 'admin/wx.reply/index', '微信自定义回复设置', 1, 0, 1, '', '2017-01-12 06:40:51', '0000-00-00 00:00:00'),
(13, 'admin/wx.reply/add', '新增修改自定义回复', 1, 0, 1, '', '2017-01-12 07:19:59', '0000-00-00 00:00:00'),
(14, 'admin/article.category/index', '文章分类', 1, 0, 1, '', '2017-01-14 01:35:20', '0000-00-00 00:00:00'),
(15, 'admin/article.category/add', '新增修改文章分类', 1, 0, 1, '', '2017-01-14 03:30:50', '0000-00-00 00:00:00'),
(16, 'admin/article.category/del', '删除文章分类', 1, 0, 1, '', '2017-01-14 03:42:44', '0000-00-00 00:00:00'),
(17, 'admin/article.index/index', '文章列表', 1, 0, 1, '', '2017-01-14 04:05:56', '0000-00-00 00:00:00'),
(18, 'admin/article.index/add', '新增修改文章', 1, 0, 1, '', '2017-01-16 01:26:29', '0000-00-00 00:00:00'),
(19, 'admin/auth.group/index', '用户组管理', 1, 0, 1, '', '2017-01-16 03:16:56', '0000-00-00 00:00:00'),
(20, 'admin/auth.group/add', '新增修改用户组', 1, 0, 1, '', '2017-01-16 03:44:14', '0000-00-00 00:00:00'),
(21, 'admin/auth.group/del', '删除用户组', 1, 0, 1, '', '2015-10-14 10:18:46', '0000-00-00 00:00:00'),
(22, 'admin/auth.group/update', '更改用户组状态', 1, 0, 1, '', '2015-10-14 10:19:00', '0000-00-00 00:00:00'),
(23, 'admin/auth.admin/index', '管理员管理', 1, 0, 1, '', '2015-10-14 06:03:58', '0000-00-00 00:00:00'),
(24, 'admin/auth.admin/add', '新增修改管理员', 1, 0, 1, '', '2015-10-14 06:03:58', '0000-00-00 00:00:00'),
(25, 'admin/auth.admin/del', '删除管理员', 1, 0, 1, '', '2015-10-14 06:03:58', '0000-00-00 00:00:00'),
(26, 'admin/auth.admin/update', '更改管理员状态', 1, 0, 1, '', '2015-10-14 10:19:56', '0000-00-00 00:00:00'),
(27, 'admin/user.index/index', '用户列表', 1, 0, 1, '', '2015-10-14 06:03:58', '0000-00-00 00:00:00'),
(28, 'admin/user.index/add', '新增修改用户', 1, 0, 1, '', '2015-10-14 06:03:58', '0000-00-00 00:00:00'),
(29, 'admin/user.index/del', '删除用户', 1, 0, 1, '', '2015-10-14 10:20:12', '0000-00-00 00:00:00'),
(30, 'admin/user.index/update', '更新用户状态', 1, 0, 1, '', '2015-10-14 10:20:18', '0000-00-00 00:00:00'),
(33, 'admin/file/index', '图片列表', 1, 0, 1, '', '2015-10-14 06:08:44', '0000-00-00 00:00:00'),
(34, 'admin/file/upload', '图片上传', 1, 0, 1, '', '2015-10-14 06:08:44', '0000-00-00 00:00:00'),
(35, 'admin/file/del', '删除图片', 1, 0, 1, '', '2015-10-14 06:08:44', '0000-00-00 00:00:00'),
(36, 'admin/shop.product.category/index', '菜单列表', 1, 0, 1, '', '2015-10-14 06:08:44', '0000-00-00 00:00:00'),
(37, 'admin/shop.product.category/add', '新增修改菜单', 1, 0, 1, '', '2015-10-18 03:09:21', '0000-00-00 00:00:00'),
(38, 'admin/shop.product.category/del', '删除菜单', 1, 0, 1, '', '2015-10-14 10:21:04', '0000-00-00 00:00:00'),
(39, 'admin/shop.product.label/index', '标签列表', 1, 0, 1, '', '2015-10-14 10:21:11', '0000-00-00 00:00:00'),
(40, 'admin/shop.product.label/add', '新增修改标签', 1, 0, 1, '', '2015-10-14 10:21:17', '0000-00-00 00:00:00'),
(41, 'admin/shop.product.label/del', '删除标签', 1, 0, 1, '', '2015-10-14 10:21:23', '0000-00-00 00:00:00'),
(42, 'admin/shop.product.index/index', '商品列表', 1, 0, 1, '', '2015-10-14 10:21:31', '0000-00-00 00:00:00'),
(43, 'admin/shop.product.index/add', '新增修改商品', 1, 0, 1, '', '2015-10-14 10:21:37', '0000-00-00 00:00:00'),
(44, 'admin/shop.product.index/update', '更新商品状态', 1, 0, 1, '', '2015-10-14 10:21:43', '0000-00-00 00:00:00'),
(45, 'admin/shop.product.index/del', '删除商品', 1, 0, 1, '', '2015-10-14 10:21:48', '0000-00-00 00:00:00'),
(46, 'admin/shop.product.sku/index', 'sku列表', 1, 0, 1, '', '2015-10-14 10:21:54', '0000-00-00 00:00:00'),
(47, 'admin/shop.product.sku/add', '新增修改sku', 1, 0, 1, '', '2015-10-14 10:22:00', '0000-00-00 00:00:00'),
(48, 'admin/shop.product.sku/del', '删除sku', 1, 0, 1, '', '2015-10-14 10:22:09', '0000-00-00 00:00:00'),
(49, 'admin/shop.product.sku/getlist', 'ajax获取sku', 1, 0, 1, '', '2015-10-14 10:22:14', '0000-00-00 00:00:00'),
(50, 'admin/config.wx/delreply', '删除自定义回复', 1, 0, 1, '', '2015-10-18 02:16:38', '0000-00-00 00:00:00'),
(51, 'admin/article.index/del', '删除文章', 1, 0, 1, '', '2015-10-14 06:11:01', '0000-00-00 00:00:00'),
(52, 'admin/shop.product.comment/index', '用户评论', 1, 0, 1, '', '2015-10-18 02:19:17', '0000-00-00 00:00:00'),
(53, 'admin/shop.product.comment/del', '删除评论', 1, 0, 1, '', '2015-10-18 02:19:35', '0000-00-00 00:00:00'),
(54, 'admin/shop.product.comment/update', '更改评论状态', 1, 0, 1, '', '2015-10-14 10:21:23', '0000-00-00 00:00:00'),
(55, 'admin/config.pay/update', '修改支付方式状态', 1, 0, 1, '', '2015-10-18 02:21:13', '0000-00-00 00:00:00'),
(56, 'admin/config.pay/add', '配置支付', 1, 0, 1, '', '2015-10-18 02:24:07', '0000-00-00 00:00:00'),
(57, 'admin/shop.order.index/index', '订单列表', 1, 0, 1, '', '2015-10-14 10:21:23', '0000-00-00 00:00:00'),
(58, 'admin/shop.order.index/detail', '订单详情', 1, 0, 1, '', '2017-01-10 13:52:32', '0000-00-00 00:00:00'),
(59, 'admin/config.delivery/index', '快递列表', 1, 0, 1, '', '2017-01-10 14:00:50', '0000-00-00 00:00:00'),
(60, 'admin/config.delivery/add', '添加修改快递', 1, 0, 1, '', '2017-02-13 18:13:26', '0000-00-00 00:00:00'),
(61, 'admin/config.delivery/del', '删除快递', 1, 0, 1, '', '2017-02-14 23:21:34', '0000-00-00 00:00:00'),
(62, 'admin/config.delivery/update', '启用禁用快递', 1, 0, 1, '', '2017-02-15 01:34:53', '0000-00-00 00:00:00'),
(63, 'admin/shop.order.index/update', '更改订单状态', 1, 0, 1, '', '2017-02-15 03:15:37', '0000-00-00 00:00:00'),
(64, 'admin/shop.product.booking/index', '缺货登记列表', 1, 0, 1, '', '2017-02-15 03:25:32', '0000-00-00 00:00:00'),
(65, 'admin/shop.order.backtype/index', '售后类型列表', 1, 0, 1, '', '2017-02-15 07:15:19', '0000-00-00 00:00:00'),
(66, 'admin/shop.order.backtype/add', '编辑添加售后类型', 1, 0, 1, '', '2017-02-15 07:30:01', '0000-00-00 00:00:00'),
(67, 'admin/shop.order.backtype/del', '删除售后类型', 1, 0, 1, '', '2017-02-15 07:30:41', '0000-00-00 00:00:00'),
(68, 'admin/shop.order.backtype/update', '更新售后类型状态', 1, 0, 1, '', '2017-02-15 07:31:09', '0000-00-00 00:00:00'),
(69, 'admin/shop.order.backlist/index', '售后列表', 1, 0, 1, '', '2017-02-15 08:05:53', '0000-00-00 00:00:00'),
(70, 'admin/shop.order.backlist/update', '更改售后状态', 1, 0, 1, '', '2017-02-15 08:29:54', '0000-00-00 00:00:00'),
(71, 'admin/user.level/index', '会员等级列表', 1, 0, 1, '', '2017-02-15 10:10:19', '0000-00-00 00:00:00'),
(72, 'admin/user.level/add', '新增编辑会员等级', 1, 0, 1, '', '2017-02-15 10:15:43', '0000-00-00 00:00:00'),
(73, 'admin/user.level/del', '删除会员等级', 1, 0, 1, '', '2017-02-15 11:18:56', '0000-00-00 00:00:00'),
(74, 'admin/user.msg/index', '消息列表', 1, 0, 1, '', '2017-02-15 11:19:21', '0000-00-00 00:00:00'),
(75, 'admin/user.msg/add', '添加修改消息', 1, 0, 1, '', '2017-02-15 12:53:51', '0000-00-00 00:00:00'),
(76, 'admin/user.msg/del', '删除消息', 1, 0, 1, '', '2017-02-15 12:54:14', '0000-00-00 00:00:00'),
(77, 'admin/user.msg/update', '更新消息状态', 1, 0, 1, '', '2017-02-15 12:54:38', '0000-00-00 00:00:00'),
(78, 'admin/user.tx/index', '提现列表', 1, 0, 1, '', '2017-02-15 13:17:49', '0000-00-00 00:00:00'),
(79, 'admin/user.tx/update', '更改提现状态', 1, 0, 1, '', '2017-02-15 13:18:23', '0000-00-00 00:00:00'),
(80, 'admin/shop.trade/index', '财务明细', 1, 0, 1, '', '2017-02-15 13:47:10', '0000-00-00 00:00:00'),
(81, 'admin/user.recharge/index', '充值记录', 1, 0, 1, '', '2017-02-15 14:28:04', '0000-00-00 00:00:00'),
(82, 'admin/config.mail/index', '邮件配置', 1, 0, 1, '', '2017-02-16 01:26:09', '0000-00-00 00:00:00'),
(83, 'admin/config.sms/index', '短信配置', 1, 0, 1, '', '2017-02-16 02:22:50', '0000-00-00 00:00:00'),
(84, 'admin/location.country/index', '地址设置', 1, 0, 1, '', '2017-02-16 02:29:01', '0000-00-00 00:00:00'),
(85, 'admin/location.country/add', '新增修改国家', 1, 0, 1, '', '2017-02-16 03:13:01', '0000-00-00 00:00:00'),
(86, 'admin/location.country/update', '更新国家状态', 1, 0, 1, '', '2017-02-16 03:13:26', '0000-00-00 00:00:00'),
(87, 'admin/location.province/index', '省份列表', 1, 0, 1, '', '2017-02-16 03:24:34', '0000-00-00 00:00:00'),
(88, 'admin/location.province/add', '新增修改省份', 1, 0, 1, '', '2017-02-16 03:24:58', '0000-00-00 00:00:00'),
(89, 'admin/location.province/update', '更新省份状态', 1, 0, 1, '', '2017-02-16 03:25:33', '0000-00-00 00:00:00'),
(90, 'admin/location.city/index', '城市列表', 1, 0, 1, '', '2017-02-16 06:13:05', '0000-00-00 00:00:00'),
(92, 'admin/location.city/add', '新增修改城市', 1, 0, 1, '', '2017-02-16 06:13:14', '0000-00-00 00:00:00'),
(93, 'admin/location.city/update', '更新城市状态', 1, 0, 1, '', '2017-02-16 06:13:20', '0000-00-00 00:00:00'),
(94, 'admin/location.district/index', '区域列表', 1, 0, 1, '', '2017-02-16 06:32:17', '0000-00-00 00:00:00'),
(95, 'admin/location.district/add', '新增修改区域', 1, 0, 1, '', '2017-02-16 06:32:20', '0000-00-00 00:00:00'),
(96, 'admin/location.district/update', '更新区域状态', 1, 0, 1, '', '2017-02-16 06:32:24', '0000-00-00 00:00:00'),
(97, 'admin/tpl.mail/index', '邮件模版列表', 1, 0, 1, '', '2017-02-16 07:05:55', '0000-00-00 00:00:00'),
(98, 'admin/tpl.mail/add', '新增修改邮件模版', 1, 0, 1, '', '2017-02-16 07:05:58', '0000-00-00 00:00:00'),
(99, 'admin/tpl.mail/update', '更新邮件模版状态', 1, 0, 1, '', '2017-02-16 07:06:02', '0000-00-00 00:00:00'),
(100, 'admin/tpl.sms/index', '短信模版列表', 1, 0, 1, '', '2017-02-16 07:06:06', '0000-00-00 00:00:00'),
(101, 'admin/tpl.sms/add', '新增修改短信模版', 1, 0, 1, '', '2017-02-16 07:30:03', '0000-00-00 00:00:00'),
(102, 'admin/tpl.sms/update', '开启关闭短信模版', 1, 0, 1, '', '2017-02-16 07:30:08', '0000-00-00 00:00:00'),
(103, 'admin/wx.tplmsg/index', '模版消息列表', 1, 0, 1, '', '2017-02-16 07:30:13', '0000-00-00 00:00:00'),
(104, 'admin/wx.tplmsg/add', '新增编辑模版消息', 1, 0, 1, '', '2017-02-16 10:43:55', '0000-00-00 00:00:00'),
(106, 'admin/wx.tplmsg/update', '开启关闭模版消息', 1, 0, 1, '', '2017-02-16 10:44:03', '0000-00-00 00:00:00'),
(107, 'admin/wx.kefu/index', '多客服设置', 1, 0, 1, '', '2017-02-16 10:44:09', '0000-00-00 00:00:00'),
(108, 'admin/wx.print/index', '微信打印机设置', 1, 0, 1, '', '2017-02-17 01:31:57', '0000-00-00 00:00:00'),
(109, 'admin/article.category/update', '更改文章分类状态', 1, 0, 1, '', '2017-02-17 02:00:42', '0000-00-00 00:00:00'),
(110, 'admin/article.index/update', '更改文章状态', 1, 0, 1, '', '2017-02-17 02:12:54', '0000-00-00 00:00:00'),
(112, 'admin/ads.position/index', '广告位置', 1, 0, 1, '', '2017-02-17 03:11:26', '0000-00-00 00:00:00'),
(113, 'admin/ads.position/update', '开启关闭广告位置', 1, 0, 1, '', '2017-02-17 03:23:45', '0000-00-00 00:00:00'),
(114, 'admin/ads.index/index', '广告列表', 1, 0, 1, '', '2017-02-17 03:31:30', '0000-00-00 00:00:00'),
(115, 'admin/ads.index/add', '新增修改广告', 1, 0, 1, '', '2017-02-17 03:34:48', '0000-00-00 00:00:00'),
(116, 'admin/ads.index/update', '开启关闭广告', 1, 0, 1, '', '2017-02-17 03:35:10', '0000-00-00 00:00:00'),
(117, 'admin/user.index/export', '导出用户', 1, 0, 1, '', '2017-02-17 06:28:31', '0000-00-00 00:00:00'),
(118, 'admin/shop.trade/export', '导出财务明细', 1, 0, 1, '', '2017-02-17 08:35:20', '0000-00-00 00:00:00'),
(119, 'admin/shop.order.index/export', '导出全部订单', 1, 0, 1, '', '2017-02-17 10:14:50', '0000-00-00 00:00:00'),
(120, 'admin/shop.product.index/export', '导出全部商品', 1, 0, 1, '', '2017-02-18 01:11:09', '0000-00-00 00:00:00'),
(121, 'admin/tpl.sms/send', '发送测试短信', 1, 0, 1, '', '2017-02-18 09:08:55', '0000-00-00 00:00:00'),
(122, 'admin/tpl.mail/send', '发送测试邮件', 1, 0, 1, '', '2017-02-18 09:24:27', '0000-00-00 00:00:00'),
(123, 'admin/shop.product.index/sku', '商品sku管理', 1, 0, 1, '', '2017-02-22 14:11:44', '0000-00-00 00:00:00'),
(124, 'admin/addons/index', '插件管理', 1, 0, 1, '', '2017-02-27 01:53:49', '0000-00-00 00:00:00'),
(125, 'admin/addons/shop', '应用商店', 1, 0, 1, '', '2017-03-01 10:09:35', '0000-00-00 00:00:00'),
(126, 'admin/addons/getFileDownload', '下载插件', 1, 0, 1, '', '2017-03-01 10:29:52', '0000-00-00 00:00:00'),
(127, 'admin/addons/compare', '解压插件', 1, 0, 1, '', '2017-03-01 10:30:32', '0000-00-00 00:00:00'),
(128, 'admin/tpl.fee/index', '费用模版', 1, 0, 1, NULL, '2017-03-13 05:46:01', '0000-00-00 00:00:00'),
(129, 'admin/tpl.fee/add', '新增修改费用模版', 1, 0, 1, NULL, '2017-03-13 05:51:57', '0000-00-00 00:00:00'),
(130, 'admin/tpl.fee/update', '更新费用模版', 1, 0, 1, NULL, '2017-03-13 05:53:08', '0000-00-00 00:00:00'),
(131, 'admin/shop.feedback/index', '反馈管理列表', 1, 0, 1, NULL, '2017-03-14 01:06:32', '0000-00-00 00:00:00'),
(132, 'admin/ads.position/add', '新增修改广告位置', 1, 0, 1, NULL, '2017-03-15 07:45:52', '0000-00-00 00:00:00'),
(133, 'admin/base/update', '系统更新', 1, 0, 1, NULL, '2017-03-20 02:57:30', '0000-00-00 00:00:00'),
(134, 'admin/wx.reply/del', '删除自定义回复', 1, 0, 1, NULL, '2017-03-21 02:57:30', '0000-00-00 00:00:00'),
(135, 'admin/wx.robot/index', '图灵机器人', 1, 0, 1, NULL, '2017-03-21 02:57:30', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- 表的结构 `config`
--

CREATE TABLE `config` (
  `id` int(10) unsigned NOT NULL,
  `name` text COMMENT '商城名称',
  `title` text COMMENT '商城标题',
  `description` text COMMENT '商城描述',
  `help` text COMMENT '使用帮助',
  `about` text COMMENT '关于我们',
  `keywords` text COMMENT '商城关键词',
  `welcome` text COMMENT '欢迎信息',
  `logo_id` int(11) NOT NULL DEFAULT '0' COMMENT '商城Logo',
  `qq` varchar(255) DEFAULT NULL COMMENT '客服QQ',
  `tel` text COMMENT '客服电话',
  `mail` text COMMENT '客服邮箱',
  `tx_fee` float NOT NULL DEFAULT '0' COMMENT '提现手续费',
  `theme` text COMMENT '模版主题',
  `debug` int(1) NOT NULL DEFAULT '0' COMMENT '调试模式',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1:开启0:关闭',
  `shop_update` int(1) NOT NULL DEFAULT '1' COMMENT '商城升级',
  `closed_reason` text COMMENT '关站原因',
  `tongji_code` text COMMENT '统计代码',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `config`
--

INSERT INTO `config` (`id`, `name`, `title`, `description`, `help`, `about`, `keywords`, `welcome`, `logo_id`, `qq`, `tel`, `mail`, `tx_fee`, `theme`, `debug`, `status`, `shop_update`, `closed_reason`, `tongji_code`, `created_at`, `updated_at`) VALUES
(1, 'wemall商城', '单用户微商城', '111111', '这里是使用帮助', '', 'wemall', 'wemall欢迎您！\r\n111', 105, '1604583867', '18538753627', '1604583867@qq.com', 0.01, 'default', 0, 1, 1, '商城暂时关闭。。。。', '111112', '2017-01-10 13:30:26', '2017-05-27 01:49:01');

-- --------------------------------------------------------

--
-- 表的结构 `delivery`
--

CREATE TABLE `delivery` (
  `id` int(10) unsigned NOT NULL,
  `name` text,
  `sub` text,
  `status` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `delivery`
--

INSERT INTO `delivery` (`id`, `name`, `sub`, `status`, `created_at`, `updated_at`) VALUES
(1, '顺丰快递', '顺丰快递介绍', 0, '2017-02-15 01:14:39', '2017-02-15 01:48:56'),
(2, '中通快递', '', 1, '2017-01-02 19:04:50', '2017-02-02 11:09:02'),
(3, '顺丰速递', '顺丰速递', 0, '2017-01-05 19:59:10', '2017-01-28 10:53:06'),
(4, '顺丰速递', '顺丰速递介绍', 1, '2017-01-05 19:59:28', '2017-02-09 23:24:22'),
(5, '天天快递', '天天快递', 0, '2017-01-07 00:24:51', '2017-02-13 00:54:36'),
(6, '申通速递', '申通速递', 0, '2017-01-12 22:23:46', '2017-02-11 15:45:44'),
(7, '顺丰快递', '顺丰快递', 0, '2017-01-12 22:55:19', '2017-02-01 02:49:04'),
(8, '同城快递', '11111111111111', 0, '2017-03-02 06:46:55', '2017-03-02 06:46:55');

-- --------------------------------------------------------

--
-- 表的结构 `feedback`
--

CREATE TABLE `feedback` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `value` text,
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `feedback`
--

INSERT INTO `feedback` (`id`, `user_id`, `value`, `remark`, `created_at`, `updated_at`) VALUES
(1, 1, '很好', NULL, '2016-12-21 04:13:36', '2016-12-21 04:13:36');

-- --------------------------------------------------------

--
-- 表的结构 `fee_tpl`
--

CREATE TABLE `fee_tpl` (
  `id` int(11) unsigned NOT NULL,
  `name` text,
  `value` float NOT NULL DEFAULT '0' COMMENT '模版费用',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '1:开启0:关闭',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `fee_tpl`
--

INSERT INTO `fee_tpl` (`id`, `name`, `value`, `status`, `created_at`, `updated_at`) VALUES
(1, '跑腿费', 5, 1, '2017-03-13 05:42:46', '2017-03-13 06:04:31'),
(2, '111', 1, 1, '2017-03-14 02:59:43', '2017-03-14 02:59:43');

-- --------------------------------------------------------

--
-- 表的结构 `file`
--

CREATE TABLE `file` (
  `id` int(10) unsigned NOT NULL,
  `name` text,
  `ext` text,
  `type` text,
  `savename` text,
  `savepath` text,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `location`
--

CREATE TABLE `location` (
  `id` smallint(5) unsigned NOT NULL,
  `pid` smallint(5) NOT NULL DEFAULT '0',
  `name` varchar(120) DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:国家，1:省份，2:市，3:区',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `location`
--

INSERT INTO `location` (`id`, `pid`, `name`, `type`, `status`) VALUES
(1, 0, '中国', 0, 0),
(2, 1, '河南', 1, 0),
(3, 2, '郑州', 2, 0),
(4, 2, '开封', 2, 0),
(5, 3, '金水区', 3, 0);

-- --------------------------------------------------------

--
-- 表的结构 `mail`
--

CREATE TABLE `mail` (
  `id` int(11) unsigned NOT NULL,
  `host` text COMMENT '服务器地址',
  `port` int(11) DEFAULT NULL COMMENT '服务器端口',
  `secure` double NOT NULL DEFAULT '0' COMMENT '1:加密0:不加密',
  `replyTo` text COMMENT '回信地址',
  `user` text COMMENT '发送邮箱',
  `pass` text COMMENT '授权码,通过QQ获取',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `mail`
--

INSERT INTO `mail` (`id`, `host`, `port`, `secure`, `replyTo`, `user`, `pass`, `created_at`, `updated_at`) VALUES
(1, 'smtpdm.aliyun.com', 465, 1, 'koahub@163.com', 'mail@notice.koahub.com', '786699892smtp', '2017-02-16 01:52:41', '2017-03-02 06:48:29');

-- --------------------------------------------------------

--
-- 表的结构 `mail_tpl`
--

CREATE TABLE `mail_tpl` (
  `id` int(11) unsigned NOT NULL,
  `type` text COMMENT '类型',
  `name` text COMMENT '模版名',
  `content` text COMMENT '内容',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '状态1:开启0:关闭',
  `mail` text COMMENT '测试发送邮箱',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `mail_tpl`
--

INSERT INTO `mail_tpl` (`id`, `type`, `name`, `content`, `status`, `mail`, `created_at`, `updated_at`) VALUES
(1, 'register', '注册模版', '<p>您好，欢迎您注册wemallshop微信商城，您的验证码是：$code</p>', 1, '1604583867@qq.com', '0000-00-00 00:00:00', '2017-02-18 09:38:44');

-- --------------------------------------------------------

--
-- 表的结构 `oauth_applet`
--

CREATE TABLE `oauth_applet` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `openid` text COMMENT '用户的标识，对当前小程序唯一',
  `nickname` text COMMENT '用户的昵称',
  `gender` int(11) NOT NULL DEFAULT '0' COMMENT '1:男2:女0:未知',
  `city` text COMMENT '用户所在城市',
  `province` text COMMENT '用户所在省份',
  `language` text COMMENT '语言',
  `avatarUrl` text COMMENT '用户头像',
  `unionid` text COMMENT '统一用户',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='微信小程序登陆';

--
-- 转存表中的数据 `oauth_applet`
--

INSERT INTO `oauth_applet` (`id`, `user_id`, `openid`, `nickname`, `gender`, `city`, `province`, `language`, `avatarUrl`, `unionid`, `created_at`, `updated_at`) VALUES
(1, 1, '', '', 0, '', '', '', '', '', '2017-03-13 13:24:02', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- 表的结构 `oauth_wx`
--

CREATE TABLE `oauth_wx` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `openid` text COMMENT '用户的标识，对当前公众号唯一',
  `nickname` text COMMENT '用户的昵称',
  `sex` int(11) NOT NULL DEFAULT '0' COMMENT '1:男2:女0:未知',
  `city` text COMMENT '用户所在城市',
  `country` text COMMENT '用户所在国家',
  `province` text COMMENT '用户所在省份',
  `language` text COMMENT '语言',
  `headimgurl` text COMMENT '用户头像',
  `subscribe_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '关注时间',
  `unionid` text COMMENT '统一用户',
  `subscribe` int(11) NOT NULL DEFAULT '1' COMMENT '1:关注0:取消',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='微信登陆';

--
-- 转存表中的数据 `oauth_wx`
--

INSERT INTO `oauth_wx` (`id`, `user_id`, `openid`, `nickname`, `sex`, `city`, `country`, `province`, `language`, `headimgurl`, `subscribe_time`, `unionid`, `subscribe`, `created_at`, `updated_at`) VALUES
(1, 1, '', '', 0, '', '', '', '', '', '0000-00-00 00:00:00', '', 0, '2017-03-13 13:24:02', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- 表的结构 `order`
--

CREATE TABLE `order` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `delivery_id` int(11) NOT NULL DEFAULT '0' COMMENT '快递方式',
  `delivery_code` text COMMENT '快递单号',
  `delivery_time` text,
  `orderid` text COMMENT '订单号',
  `totalprice` double(10,2) NOT NULL DEFAULT '0.00' COMMENT '总价',
  `totalprice_org` float NOT NULL DEFAULT '0' COMMENT '原价',
  `payment_id` int(11) NOT NULL DEFAULT '0' COMMENT '付款方式',
  `pay_status` int(11) NOT NULL DEFAULT '0' COMMENT '1:已支付0:未支付',
  `coupon` text COMMENT '优惠券',
  `freight` float NOT NULL DEFAULT '0',
  `discount` int(11) NOT NULL DEFAULT '0',
  `totalscore` int(11) NOT NULL DEFAULT '0' COMMENT '积分',
  `employee_id` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '1:到店自提0:送货上门',
  `stores` text COMMENT '自提门店',
  `remark` text COMMENT '备注',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0:待发货，1:已发货，2:已完成，3:已评价, -1:已关闭，-2:待退款，-3:已退款',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `order_contact`
--

CREATE TABLE `order_contact` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `phone` text,
  `province` text,
  `city` text,
  `district` text,
  `address` text,
  `postcode` text,
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `sku_id` text,
  `sku_name` text,
  `num` int(11) NOT NULL DEFAULT '0',
  `price` double(10,2) NOT NULL DEFAULT '0.00',
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `order_fee`
--

CREATE TABLE `order_fee` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `value` float NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `order_feedback_type`
--

CREATE TABLE `order_feedback_type` (
  `id` int(11) unsigned NOT NULL,
  `name` text COMMENT '类型',
  `sub` text COMMENT '备注',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '状态1:开启0:关闭',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `order_feedback_type`
--

INSERT INTO `order_feedback_type` (`id`, `name`, `sub`, `status`, `created_at`, `updated_at`) VALUES
(1, '不想要了1', '这里是备注', 1, '2016-12-10 03:20:38', '2017-02-15 07:39:20'),
(2, '破了', '', 1, '2016-12-13 02:29:07', '2017-01-15 22:17:33'),
(3, '111', '222', 1, '2016-12-15 15:39:03', '2017-01-15 22:17:35'),
(4, '实物差距太大', '实物差距太大', 1, '2017-01-05 22:34:51', '2017-01-15 22:17:37'),
(5, '22', '222', 1, '2017-03-02 07:06:42', '2017-03-02 07:06:50');

-- --------------------------------------------------------

--
-- 表的结构 `payment`
--

CREATE TABLE `payment` (
  `id` tinyint(3) unsigned NOT NULL,
  `type` varchar(20) DEFAULT NULL,
  `name` varchar(120) DEFAULT NULL,
  `sub` text,
  `rank` tinyint(3) NOT NULL DEFAULT '0',
  `config` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `is_cod` tinyint(1) NOT NULL DEFAULT '0' COMMENT '货到付款',
  `is_online` tinyint(1) NOT NULL DEFAULT '0' COMMENT '在线支付',
  `is_config` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `payment`
--

INSERT INTO `payment` (`id`, `type`, `name`, `sub`, `rank`, `config`, `status`, `is_cod`, `is_online`, `is_config`, `created_at`, `updated_at`) VALUES
(1, 'cod', '货到付款', '开通城市请详见网站公告。', 0, '', 1, 1, 0, 0, '2017-02-14 05:11:48', '2017-02-13 21:04:55'),
(2, 'wxpay', '微信支付', '微信支付，是基于微信公众号JS_API网页支付提供的支付服务功能。', 0, '{"mchid":"1243051102","key":"0IVTlxNMurPnBga1ulahd7g4m36jirgR","x_mchid":"1445117902","x_key":"8e157d6843fb72dcb27f9762308b8333"}', 1, 0, 1, 1, '2017-02-14 05:11:48', '2017-03-07 02:36:01'),
(3, 'alipay', '支付宝', '支付宝(www.alipay.com)是国内先进的网上支付平台。', 0, '{"account":"koahub@163.com","partner":"2088421387324241","key":"95ljtrm73l34qg1x5dlfyqpayx2c1c0v"}', 1, 0, 1, 1, '2017-02-14 05:11:48', '2017-02-14 05:47:00'),
(4, 'balance', '余额支付', '使用帐户余额支付。只有会员才能使用，通过设置信用额度，可以透支。', 0, '', 1, 0, 1, 0, '2017-02-14 05:11:48', '2017-02-13 02:10:32');

-- --------------------------------------------------------

--
-- 表的结构 `product`
--

CREATE TABLE `product` (
  `id` int(10) unsigned NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `subname` text,
  `price` float NOT NULL DEFAULT '0' COMMENT '售价',
  `old_price` float NOT NULL DEFAULT '0' COMMENT '原价',
  `spec` text COMMENT '商品规格',
  `address` text COMMENT '商品产地',
  `store` int(11) NOT NULL DEFAULT '0' COMMENT '库存',
  `sales` int(11) NOT NULL DEFAULT '0' COMMENT '销量',
  `unit` text COMMENT '商品单位',
  `score` float NOT NULL DEFAULT '0' COMMENT '赠送积分',
  `visiter` int(11) NOT NULL DEFAULT '0' COMMENT '访问量',
  `sku` text,
  `sku_status` int(11) NOT NULL DEFAULT '0' COMMENT '1:启用0:禁用',
  `file_id` int(11) NOT NULL DEFAULT '0' COMMENT '图片',
  `files` text COMMENT '图集',
  `detail` text,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '0:隐藏，1:显示',
  `labels` text COMMENT '标签值',
  `remark` text,
  `rank` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `product_category`
--

CREATE TABLE `product_category` (
  `id` int(11) unsigned NOT NULL,
  `name` text,
  `pid` int(11) NOT NULL DEFAULT '0',
  `file_id` int(11) NOT NULL DEFAULT '0',
  `remark` text,
  `rank` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `product_comment`
--

CREATE TABLE `product_comment` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `star` int(11) NOT NULL DEFAULT '0' COMMENT '星星5',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '1:显示0:隐藏',
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `product_exchange`
--

CREATE TABLE `product_exchange` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `reason_id` int(11) NOT NULL DEFAULT '0',
  `remark` text,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '-1:拒绝0:未处理1:通过',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `product_label`
--

CREATE TABLE `product_label` (
  `id` int(10) unsigned NOT NULL,
  `name` text,
  `subname` text,
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `product_sku`
--

CREATE TABLE `product_sku` (
  `id` int(11) unsigned NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品id',
  `ids` text COMMENT 'sku-id',
  `name` text COMMENT 'sku-name',
  `price` float NOT NULL DEFAULT '0' COMMENT '售价',
  `old_price` float NOT NULL DEFAULT '0' COMMENT '原价',
  `store` int(11) NOT NULL DEFAULT '0' COMMENT '库存',
  `sales` int(11) NOT NULL DEFAULT '0' COMMENT '销量',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `sku`
--

CREATE TABLE `sku` (
  `id` int(10) unsigned NOT NULL,
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '上级id',
  `text` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `sku`
--

INSERT INTO `sku` (`id`, `pid`, `text`, `created_at`, `updated_at`) VALUES
(1, 0, '颜色', '2016-11-28 02:22:44', '2017-01-18 09:51:30'),
(2, 1, '红', '2016-11-28 02:22:44', '2017-02-08 13:41:29'),
(3, 1, '黄', '2017-01-18 02:37:36', '2017-01-18 02:38:00'),
(4, 1, '蓝', '2017-01-18 02:37:36', '2017-01-18 02:38:00'),
(5, 0, '尺寸', '2017-01-18 02:37:36', '2017-01-18 02:38:00'),
(6, 5, 'X', '2017-01-18 02:37:36', '2017-01-18 02:38:00'),
(7, 5, 'XL', '2017-01-18 02:37:36', '2017-01-18 02:38:00'),
(8, 5, '2XL', '2017-01-18 02:37:36', '2017-01-18 02:38:00'),
(9, 0, '大小', '2017-01-18 02:37:36', '2017-01-18 02:38:00'),
(10, 9, '大', '2017-01-18 02:37:36', '2017-01-18 02:38:00'),
(11, 9, '小', '2017-01-18 02:37:36', '2017-01-18 02:38:00');

-- --------------------------------------------------------

--
-- 表的结构 `sms`
--

CREATE TABLE `sms` (
  `id` int(10) unsigned NOT NULL,
  `app_key` text,
  `app_secret` text,
  `sign` text COMMENT '短信签名',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `sms`
--

INSERT INTO `sms` (`id`, `app_key`, `app_secret`, `sign`, `created_at`, `updated_at`) VALUES
(1, '23643042', '17f701feb8fd1a0f3c376d4eaaa2710b', 'tp商城', '2016-07-19 09:38:40', '2017-03-02 06:48:05');

-- --------------------------------------------------------

--
-- 表的结构 `sms_tpl`
--

CREATE TABLE `sms_tpl` (
  `id` int(11) unsigned NOT NULL,
  `type` text COMMENT '类型',
  `name` text COMMENT '模版名',
  `template_code` text COMMENT '模版ID',
  `content` text COMMENT '内容',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '状态1:开启0:关闭',
  `phone` text COMMENT '测试发送邮箱',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `sms_tpl`
--

INSERT INTO `sms_tpl` (`id`, `type`, `name`, `template_code`, `content`, `status`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'register', '短信验证码', 'SMS_47900069', '您的本次验证码${code}，10分钟内输入有效，感谢使用平台', 1, '15238027761', '0000-00-00 00:00:00', '2017-02-18 09:13:21');

-- --------------------------------------------------------

--
-- 表的结构 `sms_verify`
--

CREATE TABLE `sms_verify` (
  `id` int(11) unsigned NOT NULL,
  `uuid` text,
  `phone` text,
  `code` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `trade`
--

CREATE TABLE `trade` (
  `id` int(10) unsigned NOT NULL,
  `tradeid` text,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `payment` text COMMENT '支付方式',
  `money` float(10,2) NOT NULL DEFAULT '0.00',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '1:充值0:消费',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '1:成功0:失败',
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL,
  `contact_id` int(11) NOT NULL DEFAULT '0' COMMENT '默认地址',
  `avater_id` int(11) NOT NULL DEFAULT '0' COMMENT '头像',
  `nickname` text,
  `username` text,
  `phone` text,
  `password` text,
  `token` text,
  `money` float NOT NULL DEFAULT '0',
  `score` float NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1:启用0:禁用',
  `buy_num` int(11) NOT NULL DEFAULT '0' COMMENT '用户购买量',
  `remark` text,
  `last_login_ip` text,
  `last_login_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`id`, `contact_id`, `avater_id`, `nickname`, `username`, `phone`, `password`, `token`, `money`, `score`, `status`, `buy_num`, `remark`, `last_login_ip`, `last_login_time`, `created_at`, `updated_at`) VALUES
(1, 3, 78, NULL, 'wemall', '1', 'c4ca4238a0b923820dcc509a6f75849b', '', 2, 420, 1, 192, '12', '192.168.0.120', '2017-03-08 03:17:11', '2016-07-26 02:14:20', '2017-03-08 03:17:11');

-- --------------------------------------------------------

--
-- 表的结构 `user_collection`
--

CREATE TABLE `user_collection` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) DEFAULT '0',
  `product_id` text CHARACTER SET latin1,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_contact`
--

CREATE TABLE `user_contact` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` text,
  `phone` text,
  `country_id` int(11) NOT NULL DEFAULT '1' COMMENT '国家',
  `province_id` int(11) NOT NULL DEFAULT '0' COMMENT '省份',
  `city_id` int(11) NOT NULL DEFAULT '0' COMMENT '城市',
  `district_id` int(11) DEFAULT NULL COMMENT '区域',
  `address` text COMMENT '详细地址',
  `remark` text COMMENT '备注',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1:开启0:关闭',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_level`
--

CREATE TABLE `user_level` (
  `id` int(11) unsigned NOT NULL,
  `name` text,
  `score` float NOT NULL DEFAULT '0' COMMENT '达到积分',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user_level`
--

INSERT INTO `user_level` (`id`, `name`, `score`, `created_at`, `updated_at`) VALUES
(1, '基础会员', 0, '2017-02-15 09:58:33', '2017-03-02 09:42:02'),
(2, '初级会员', 50, '2016-12-26 15:53:37', '2017-02-15 10:37:28'),
(3, '白金会员', 100, '2017-01-05 23:03:20', '2017-02-15 10:37:37'),
(4, '铂金会员', 500, '2017-01-05 23:05:46', '2017-02-15 10:37:53'),
(5, '黄金会员', 1000, '2017-03-02 07:12:29', '2017-03-02 07:13:00');

-- --------------------------------------------------------

--
-- 表的结构 `user_msg`
--

CREATE TABLE `user_msg` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_tx`
--

CREATE TABLE `user_tx` (
  `id` int(10) unsigned NOT NULL,
  `txid` text,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `account` text COMMENT '提现账户',
  `money` float NOT NULL DEFAULT '0',
  `fee` float NOT NULL DEFAULT '0' COMMENT '手续费',
  `tx` float NOT NULL DEFAULT '0' COMMENT '最终提现',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0:未审核，1:通过，2:完成，-1:拒绝',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `wx_config`
--

CREATE TABLE `wx_config` (
  `id` int(5) unsigned NOT NULL,
  `token` text,
  `appid` text,
  `appsecret` text,
  `encodingaeskey` text,
  `x_appid` text COMMENT '小程序',
  `x_appsecret` text COMMENT '小程序',
  `old_id` text COMMENT '原始id',
  `switch` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `wx_config`
--

INSERT INTO `wx_config` (`id`, `token`, `appid`, `appsecret`, `encodingaeskey`, `x_appid`, `x_appsecret`, `old_id`, `switch`, `created_at`, `updated_at`) VALUES
(1, 'wemall', 'wx6d040141df50d2a3', '523c93731918e8476654ca8f73133824', 'vkG6JOKy7f2f1nejqJFlOJkjJEK5JJlNaJjjSQ6Q2gM', 'wx5f1a51823b837fe8', '8e157d6823fb72dcb17f9762308b8333', 'gh_6f79b1a839f6', 1, '2016-01-05 02:16:16', '2017-03-15 06:23:00');

-- --------------------------------------------------------

--
-- 表的结构 `wx_kefu`
--

CREATE TABLE `wx_kefu` (
  `id` int(5) unsigned NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `kefu` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `wx_kefu`
--

INSERT INTO `wx_kefu` (`id`, `status`, `kefu`, `created_at`, `updated_at`) VALUES
(1, 0, 'biyuehun', '0000-00-00 00:00:00', '2017-03-02 06:50:24');

-- --------------------------------------------------------

--
-- 表的结构 `wx_menu`
--

CREATE TABLE `wx_menu` (
  `id` int(5) unsigned NOT NULL,
  `pid` int(5) NOT NULL DEFAULT '0',
  `type` text,
  `name` text,
  `key` text,
  `url` text,
  `rank` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `wx_menu`
--

INSERT INTO `wx_menu` (`id`, `pid`, `type`, `name`, `key`, `url`, `rank`, `status`, `remark`, `created_at`, `updated_at`) VALUES
(1, 0, 'view', '商业版', '111', 'http://www.wemallshop.com/wemall/index.php/App/Index/index', '1', 0, '2', '2016-02-18 06:46:22', '2017-01-11 10:28:43'),
(2, 1, 'view', '分销版', '', 'http://www.wemallshop.com/wfx/App/Shop/index', '4', 0, '', '2015-11-06 09:25:28', '2017-05-27 01:52:14'),
(3, 0, 'click', 'QQ客服', 'qqkf', '', '3', 0, '2034210985', '2015-12-31 08:19:22', '2017-01-12 06:38:34');

-- --------------------------------------------------------

--
-- 表的结构 `wx_print`
--

CREATE TABLE `wx_print` (
  `id` int(11) unsigned NOT NULL,
  `apikey` varchar(100) DEFAULT NULL COMMENT 'apikey',
  `mkey` varchar(100) DEFAULT NULL COMMENT '秘钥',
  `partner` varchar(100) DEFAULT NULL COMMENT '用户id',
  `machine_code` varchar(100) DEFAULT NULL COMMENT '机器码',
  `switch` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `wx_print`
--

INSERT INTO `wx_print` (`id`, `apikey`, `mkey`, `partner`, `machine_code`, `switch`, `created_at`, `updated_at`) VALUES
(1, '61', '31', '16', '16', 0, '2016-08-07 11:49:22', '2017-03-02 06:50:36');

-- --------------------------------------------------------

--
-- 表的结构 `wx_reply`
--

CREATE TABLE `wx_reply` (
  `id` int(10) unsigned NOT NULL,
  `type` text,
  `title` text,
  `description` text,
  `file_id` int(11) NOT NULL DEFAULT '0',
  `url` text,
  `key` text,
  `remark` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `wx_reply`
--

INSERT INTO `wx_reply` (`id`, `type`, `title`, `description`, `file_id`, `url`, `key`, `remark`, `created_at`, `updated_at`) VALUES
(1, 'news', '恭喜你加入WeMall，欢迎体验WeMall商业版，WeMall分销版和WeMall开源版。WeMall商业版更新，速度提升30%，致力于打造世界上最快，体验最好的微商城。客服QQ：2034210985', '1111', 29, '', 'subscribe', '1212', '2016-01-05 02:19:53', '2017-03-02 06:49:04'),
(2, 'news', '欢迎来到商业版wemall商城', '欢迎来到商业版wemall商城11111', 103, 'http://www.wemallshop.com/3/App/Index/index', '商城', '', '2016-01-05 02:23:41', '2017-03-02 06:49:49'),
(3, 'news', '2222222', '111', 103, '1111', '11111', '1111', '2017-01-12 09:27:57', '2017-03-02 06:49:41');

-- --------------------------------------------------------

CREATE TABLE `robot` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `apikey` varchar(64) DEFAULT NULL,
  `userid` varchar(64) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1开启，0关闭',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 表的结构 `wx_tplmsg`
--

CREATE TABLE `wx_tplmsg` (
  `id` int(10) unsigned NOT NULL,
  `name` text,
  `type` text,
  `title` text,
  `status` int(11) NOT NULL DEFAULT '0',
  `remark` text,
  `template_id_short` text,
  `template_id` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `wx_tplmsg`
--

INSERT INTO `wx_tplmsg` (`id`, `name`, `type`, `title`, `status`, `remark`, `template_id_short`, `template_id`, `created_at`, `updated_at`) VALUES
(1, '订单提醒(新订单通知)', 'order', '尊敬的客户,您的订单已成功提交！', 1, '666', 'OPENTM201785396', '', '2016-08-07 11:50:16', '2017-01-12 23:07:32'),
(2, '支付提醒(订单支付成功通知)', 'pay', '您已成功支付', 1, '3333', 'OPENTM207791277', '', '2016-08-07 11:50:16', '2017-01-12 23:07:35'),
(3, '发货提醒(订单发货提醒)', 'delivery', '尊敬的客户,您的订单已发货！', 1, '33312', 'OPENTM207763419', '', '2016-08-07 11:50:16', '2017-02-16 11:04:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ads_position`
--
ALTER TABLE `ads_position`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `analysis`
--
ALTER TABLE `analysis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article_category`
--
ALTER TABLE `article_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_group_access`
--
ALTER TABLE `auth_group_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery`
--
ALTER TABLE `delivery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_tpl`
--
ALTER TABLE `fee_tpl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`pid`),
  ADD KEY `region_type` (`type`);

--
-- Indexes for table `mail`
--
ALTER TABLE `mail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mail_tpl`
--
ALTER TABLE `mail_tpl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_applet`
--
ALTER TABLE `oauth_applet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_wx`
--
ALTER TABLE `oauth_wx`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_contact`
--
ALTER TABLE `order_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_fee`
--
ALTER TABLE `order_fee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_feedback_type`
--
ALTER TABLE `order_feedback_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pay_code` (`type`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_comment`
--
ALTER TABLE `product_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_exchange`
--
ALTER TABLE `product_exchange`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_label`
--
ALTER TABLE `product_label`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_sku`
--
ALTER TABLE `product_sku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sku`
--
ALTER TABLE `sku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms`
--
ALTER TABLE `sms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_tpl`
--
ALTER TABLE `sms_tpl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_verify`
--
ALTER TABLE `sms_verify`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trade`
--
ALTER TABLE `trade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_collection`
--
ALTER TABLE `user_collection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_contact`
--
ALTER TABLE `user_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_level`
--
ALTER TABLE `user_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_msg`
--
ALTER TABLE `user_msg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_tx`
--
ALTER TABLE `user_tx`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wx_config`
--
ALTER TABLE `wx_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wx_kefu`
--
ALTER TABLE `wx_kefu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wx_menu`
--
ALTER TABLE `wx_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wx_print`
--
ALTER TABLE `wx_print`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wx_reply`
--
ALTER TABLE `wx_reply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wx_tplmsg`
--
ALTER TABLE `wx_tplmsg`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ads_position`
--
ALTER TABLE `ads_position`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `analysis`
--
ALTER TABLE `analysis`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `article_category`
--
ALTER TABLE `article_category`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `auth_group_access`
--
ALTER TABLE `auth_group_access`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `auth_rule`
--
ALTER TABLE `auth_rule`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `delivery`
--
ALTER TABLE `delivery`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fee_tpl`
--
ALTER TABLE `fee_tpl`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `mail`
--
ALTER TABLE `mail`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mail_tpl`
--
ALTER TABLE `mail_tpl`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oauth_applet`
--
ALTER TABLE `oauth_applet`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oauth_wx`
--
ALTER TABLE `oauth_wx`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_contact`
--
ALTER TABLE `order_contact`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_fee`
--
ALTER TABLE `order_fee`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_feedback_type`
--
ALTER TABLE `order_feedback_type`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_comment`
--
ALTER TABLE `product_comment`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_exchange`
--
ALTER TABLE `product_exchange`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_label`
--
ALTER TABLE `product_label`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_sku`
--
ALTER TABLE `product_sku`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sku`
--
ALTER TABLE `sku`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sms`
--
ALTER TABLE `sms`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sms_tpl`
--
ALTER TABLE `sms_tpl`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sms_verify`
--
ALTER TABLE `sms_verify`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trade`
--
ALTER TABLE `trade`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_collection`
--
ALTER TABLE `user_collection`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_contact`
--
ALTER TABLE `user_contact`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_level`
--
ALTER TABLE `user_level`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_msg`
--
ALTER TABLE `user_msg`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_tx`
--
ALTER TABLE `user_tx`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wx_config`
--
ALTER TABLE `wx_config`
  MODIFY `id` int(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wx_kefu`
--
ALTER TABLE `wx_kefu`
  MODIFY `id` int(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wx_menu`
--
ALTER TABLE `wx_menu`
  MODIFY `id` int(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wx_print`
--
ALTER TABLE `wx_print`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wx_reply`
--
ALTER TABLE `wx_reply`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wx_tplmsg`
--
ALTER TABLE `wx_tplmsg`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
